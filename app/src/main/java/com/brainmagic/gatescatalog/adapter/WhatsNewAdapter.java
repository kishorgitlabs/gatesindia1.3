package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.gates.R;

import java.util.List;

public class WhatsNewAdapter extends ArrayAdapter {
    private Context context;
    public WhatsNewAdapter(@NonNull Context context, List<String> oem, List<String> vehicleModel) {
        super(context, R.layout.adapter_whats_new);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_whats_new,parent,false);

        return view;
    }
}
