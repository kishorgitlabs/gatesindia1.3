package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.api.models.moredetails.EngineCodeResult;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;


public class Product_Grid_Adapter extends ArrayAdapter<String> {

    private Context context;
    private ArrayList<String> engine_codeList, model_codeList, year_fromList, year_tillList, month_fromList, month_tillList, stokeList;
    private String navistg, model, oem;
    private ArrayList<Integer> engineCodeList;
    private List<EngineCodeResult> data, tempData, backUpData;



    public Product_Grid_Adapter(Context context, List<EngineCodeResult> data) {
        super(context, R.layout.adapter_product_grid);
        this.context = context;
        this.data = data;
        backUpData = new ArrayList<>(data);
        tempData =new ArrayList<>();
    }

    @Override
    public int getCount() {
        if(data!=null)
            return data.size();
        else
            return 0;
    }

    public void setYearFromList(int yearFrom, String yearTill)
    {
        if("All".equals(yearTill)) {
            tempData.clear();
            for (EngineCodeResult result : backUpData) {
                if (!TextUtils.isEmpty(result.getYearFrom()) && result.getYearFrom() != null) {
                    if (Integer.parseInt(result.getYearFrom()) >= yearFrom)
                        tempData.add(result);
                }

            }
        }
        else {
            List<EngineCodeResult> dataTemp = new ArrayList<>(backUpData);
            List<EngineCodeResult> newDataTemp = new ArrayList<>();
            tempData.clear();
            for (EngineCodeResult result : dataTemp) {
                if ((!TextUtils.isEmpty(result.getYearTill()) && result.getYearTill() != null)) {
                    if (Integer.parseInt(result.getYearTill()) <= Integer.parseInt(yearTill))
                        newDataTemp.add(result);
                }
            }

            for(EngineCodeResult result : newDataTemp)
            {
                if ((!TextUtils.isEmpty(result.getYearFrom()) && result.getYearFrom() != null)) {
                    if (Integer.parseInt(result.getYearFrom()) >= (yearFrom))
                        tempData.add(result);
                }
            }

//            List<EngineCodeResult> dataTemp = new ArrayList<>(tempData);
//            tempData.clear();
//            for (EngineCodeResult result : dataTemp) {
//                if(!TextUtils.isEmpty(result.getYearFrom()) && result.getYearFrom() != null) {
//                    if (Integer.parseInt(result.getYearFrom()) >= (yearFrom)) {
//                        tempData.add(result);
//                    }
//                }
//            }
        }
        data.clear();
        data.addAll(new LinkedHashSet<>(tempData));
        notifyDataSetChanged();
    }

    public void setListBasedYearFrom(String yearFrom)
    {
        tempData.clear();
        for (EngineCodeResult result : backUpData) {
                if (!TextUtils.isEmpty(result.getYearFrom()) && result.getYearFrom() != null) {
                    if (Integer.parseInt(result.getYearFrom()) >= Integer.parseInt(yearFrom))
                        tempData.add(result);
                }
            }

        data.clear();
        data.addAll(new LinkedHashSet<>(tempData));
        notifyDataSetChanged();

    }


    public void setListBasedYearTo(String yearTill)
    {
        tempData.clear();
        for (EngineCodeResult result : backUpData) {
            if ((!TextUtils.isEmpty(result.getYearTill()) && result.getYearTill() != null) ) {
                if (Integer.parseInt(result.getYearTill()) <= Integer.parseInt(yearTill))
                    tempData.add(result);
            }
        }
        data.clear();
        data.addAll(new LinkedHashSet<>(tempData));
        notifyDataSetChanged();
    }

    public void setYearToList(String yearTill, String yearFrom)
    {
        if("All".equals(yearFrom)) {
            tempData.clear();
            for (EngineCodeResult result : backUpData) {
                if (!TextUtils.isEmpty(result.getYearTill()) && result.getYearTill() != null) {
                    if (Integer.parseInt(result.getYearTill()) <= Integer.parseInt(yearTill))
                        tempData.add(result);
                }
            }
        }
        else {

            List<EngineCodeResult> dataTemp = new ArrayList<>(backUpData);
            List<EngineCodeResult> newDataTemp = new ArrayList<>();
            tempData.clear();
            for (EngineCodeResult result : dataTemp) {
                if ((!TextUtils.isEmpty(result.getYearFrom()) && result.getYearFrom() != null)) {
                    if (Integer.parseInt(result.getYearFrom()) >= Integer.parseInt(yearFrom))
                        newDataTemp.add(result);
                }
            }

            for(EngineCodeResult result : newDataTemp)
            {
                if ((!TextUtils.isEmpty(result.getYearTill()) && result.getYearTill() != null)) {
                     if (Integer.parseInt(result.getYearTill()) <= Integer.parseInt(yearTill))
                            tempData.add(result);
                     }
            }

//            setYearFromList(Integer.parseInt(yearFrom),yearTill);
//            List<EngineCodeResult> dataTemp = new ArrayList<>(tempData);
//            tempData.clear();
//            for (EngineCodeResult result : dataTemp) {
//                if ((!TextUtils.isEmpty(result.getYearTill()) && result.getYearTill() != null)) {
//                     if (Integer.parseInt(result.getYearTill()) <= Integer.parseInt(yearTill))
//                            tempData.add(result);
//                     }
//
//            }
        }
        data.clear();
        data.addAll(new LinkedHashSet<>(tempData));
        notifyDataSetChanged();
    }

    public void setOverAllList()
    {
        data.clear();
        data.addAll(backUpData);
        notifyDataSetChanged();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        OrderHolder holder;

//        convertView = null;
        if (convertView == null) {
            if(data!=null)
                if(data.size()!=0)
                {
                    try {
                        convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                                inflate(R.layout.adapter_product_grid, parent, false);

                        holder = new OrderHolder();
                        holder.modelcodetxt = (TextView) convertView.findViewById(R.id.modelcodetxt);
                        holder.yearfromtxt = (TextView) convertView.findViewById(R.id.yearfromtxt);
                        // holder.monthfromtxt = (TextView) convertView.findViewById(R.id.monthfromtxt);
                        holder.yeartilltxt = (TextView) convertView.findViewById(R.id.yeartilltxt);
                        // holder.monthtilltxt = (TextView) convertView.findViewById(R.id.monthtilltxt);
                        holder.enginecodetxt = (TextView) convertView.findViewById(R.id.enginecodetxt);
                        holder.storketxt = (TextView) convertView.findViewById(R.id.storketxt);
//                        holder.engineNew=convertView.findViewById(R.id.cart_badge_engine);

                        /**
                         * Eg.GJEA46BB/GJEA46BC
                         * Here in engine code, value after slash should be in single line. But values splits and shows in two lines.
                         * To avoid this below code is used to remove the issue
                         *
                         */

                        holder.modelcodetxt.setText(data.get(position).getModelcode());
                        holder.enginecodetxt.setText(data.get(position).getEnginecode());
                        holder.yearfromtxt.setText(data.get(position).getYearFrom());
                        holder.yeartilltxt.setText(data.get(position).getYearTill());
//                        String model="";
//                        if(model_codeList.get(position).contains("/"))
//                        {
//                            String modelSplit[]=model_codeList.get(position).split("/");
//                            for(String tempModel:modelSplit)
//                            {
//                                if(TextUtils.isEmpty(model))
//                                        model+=tempModel;
//                                else {
//                                    model+="/\n"+tempModel;
//                                }
//                            }
//                        }
//                        else {
//                            model=model_codeList.get(position);
//                        }
//
//
//                        String engineCode="";
//
//                        if(engine_codeList.get(position).contains("/"))
//                        {
//                            String engineSplit[]=engine_codeList.get(position).split("/");
//                            for(String tempEngine:engineSplit)
//                            {
//                                if(TextUtils.isEmpty(engineCode))
//                                    engineCode+=tempEngine;
//                                else {
//                                    engineCode+="/\n"+tempEngine;
//                                }
//                            }
//                        }
//                        else {
//                            engineCode=engine_codeList.get(position);
//                        }
////                        holder.modelcodetxt.setText(model_codeList.get(position));
//                        holder.modelcodetxt.setText(model);
//                        holder.yearfromtxt.setText(year_fromList.get(position));
//                        // holder.monthfromtxt.setText(month_fromList.get(position));
//                        holder.yeartilltxt.setText(year_tillList.get(position));
//                        //  holder.monthtilltxt.setText(month_tillList.get(position));
////                        holder.enginecodetxt.setText(engine_codeList.get(position));
//                        holder.enginecodetxt.setText(engineCode);
////                        holder.storketxt.setText(stokeList.get(position));
//
//                        holder.enginecodetxt.setPaintFlags(holder.enginecodetxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//
//                        holder.modelcodetxt.setPaintFlags(holder.modelcodetxt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
//
//                        if(engineCodeList!=null)
//                        {
//                            if(engineCodeList.get(position)>0)
//                            {
//                                holder.engineNew.setVisibility(View.VISIBLE);
////                                holder.engineNew.setText(engineCodeList.get(position)+"");
//                                //Here Client needs only 'N' as shown in Ios O.s
//                                holder.engineNew.setText("N");
//                            }
//                        }


                /*holder.more_info = (ImageView) convertView.findViewById(R.id.more_info);

                holder.more_info.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent a = new Intent(context, Product_Details_Activity.class);

                        a.putExtra("modelcode", model_codeList.get(position).toString());
                        a.putExtra("model", model);
                        a.putExtra("oem", oem);
                        a.putExtra("navtxt", navistg);
                        context.startActivity(a);
                    }
                });
    */
                        convertView.setTag(holder);
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
        } else {
            holder = (OrderHolder) convertView.getTag();
            holder.modelcodetxt = (TextView) convertView.findViewById(R.id.modelcodetxt);
            holder.yearfromtxt = (TextView) convertView.findViewById(R.id.yearfromtxt);
            // holder.monthfromtxt = (TextView) convertView.findViewById(R.id.monthfromtxt);
            holder.yeartilltxt = (TextView) convertView.findViewById(R.id.yeartilltxt);
            // holder.monthtilltxt = (TextView) convertView.findViewById(R.id.monthtilltxt);
            holder.enginecodetxt = (TextView) convertView.findViewById(R.id.enginecodetxt);
            holder.storketxt = (TextView) convertView.findViewById(R.id.storketxt);
//                        holder.engineNew=convertView.findViewById(R.id.cart_badge_engine);

            /**
             * Eg.GJEA46BB/GJEA46BC
             * Here in engine code, value after slash should be in single line. But values splits and shows in two lines.
             * To avoid this below code is used to remove the issue
             *
             */

            holder.modelcodetxt.setText(data.get(position).getModelcode());
            holder.enginecodetxt.setText(data.get(position).getEnginecode());
            holder.yearfromtxt.setText(data.get(position).getYearFrom());
            holder.yeartilltxt.setText(data.get(position).getYearTill());
        }

        return convertView;
    }

}


class OrderHolder {
    TextView yearfromtxt;
    TextView monthfromtxt;
    TextView yeartilltxt;
    TextView monthtilltxt;
    TextView enginecodetxt;
    TextView storketxt, modelcodetxt,engineNew;
    ImageView more_info;


}
