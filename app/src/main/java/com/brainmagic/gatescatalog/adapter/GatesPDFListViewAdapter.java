package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.api.models.moredetails.PDFModel;

import java.util.List;


public class GatesPDFListViewAdapter extends ArrayAdapter implements Filterable {

    Context context;
    List<String> n_name;

    public GatesPDFListViewAdapter(Context context, List<String> n_name) {
        super(context, R.layout.pdf_layout_adapter, n_name);
        this.context = context;
        this.n_name = n_name;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return n_name.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ListHolder holder = new ListHolder();
        convertView = null;
        if (convertView == null) {

            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.pdf_layout_adapter, parent, false);

        }
            holder.no_name = (TextView) convertView.findViewById(R.id.pdf_view);
            holder.no_name.setText(n_name.get(position));

        return convertView;
    }


 class ListHolder
    {
        public TextView no_name;
    }
}


