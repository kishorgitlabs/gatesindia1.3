package com.brainmagic.gatescatalog.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.brainmagic.gatescatalog.api.models.PriceList.OePartList;
import com.brainmagic.gatescatalog.api.models.PriceList.ProductOeList;
import com.brainmagic.gatescatalog.gates.R;

import java.util.List;

public class ProducteOEAdapter extends ArrayAdapter {

    Context context;
    List<OePartList> data;
    List<ProductOeList>datas;

    public ProducteOEAdapter(@NonNull Context context, List<OePartList> data) {
        super(context, R.layout.productoeadapter);
        this.context=context;
        this.data=data;
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(R.layout.productoeadapter,parent,false);

        TextView partno=view.findViewById(R.id.partno);
        TextView catalogno=view.findViewById(R.id.catalogpartno);
        TextView mrp=view.findViewById(R.id.mrp);
//        TextView insertdate=view.findViewById(R.id.insertdate);

        partno.setText(data.get(position).getPartno());
        catalogno.setText(data.get(position).getCatalogPartnumber());
        mrp.setText(data.get(position).getMRP());
//        String[] a=data.get(position).getInsertdate().split("T");


//        insertdate.setText(a[0]);

return view;

    }
    public int getCount() {
        return data.size();
    }

}
