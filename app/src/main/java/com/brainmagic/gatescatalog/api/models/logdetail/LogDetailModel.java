
package com.brainmagic.gatescatalog.api.models.logdetail;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class LogDetailModel {

    @SerializedName("data")
    private LogDetailResult mData;
    @SerializedName("result")
    private String mResult;

    public LogDetailResult getData() {
        return mData;
    }

    public void setData(LogDetailResult data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
