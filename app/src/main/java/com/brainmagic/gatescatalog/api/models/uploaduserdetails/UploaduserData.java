
package com.brainmagic.gatescatalog.api.models.uploaduserdetails;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class UploaduserData {

    @SerializedName("Date")
    private String mDate;
    @SerializedName("id")
    private Long mId;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("Images")
    private String mImages;
    @SerializedName("Location")
    private String mLocation;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("Name")
    private String mName;
    @SerializedName("UserType")
    private String mUserType;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getImages() {
        return mImages;
    }

    public void setImages(String images) {
        mImages = images;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

}
