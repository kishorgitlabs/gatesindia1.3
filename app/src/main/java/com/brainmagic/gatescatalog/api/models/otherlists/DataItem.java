package com.brainmagic.gatescatalog.api.models.otherlists;

import java.io.Serializable;

public class DataItem implements Serializable {
	private String CC;
	private String KW;
	private Object Stroke;
	private String Fuel;


	public String getCC() {
		return CC;
	}

	public void setCC(String CC) {
		this.CC = CC;
	}

	public String getKW() {
		return KW;
	}

	public void setKW(String KW) {
		this.KW = KW;
	}

	public Object getStroke() {
		return Stroke;
	}

	public void setStroke(Object stroke) {
		Stroke = stroke;
	}

	public String getFuel() {
		return Fuel;
	}

	public void setFuel(String fuel) {
		Fuel = fuel;
	}

	@Override
	public String toString() {
		return "DataItem{" +
				"CC='" + CC + '\'' +
				", KW='" + KW + '\'' +
				", Stroke=" + Stroke +
				", Fuel='" + Fuel + '\'' +
				'}';
	}
}