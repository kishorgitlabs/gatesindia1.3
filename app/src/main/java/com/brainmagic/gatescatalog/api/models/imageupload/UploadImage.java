
package com.brainmagic.gatescatalog.api.models.imageupload;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class UploadImage {

    @SerializedName("Message")
    private String mMessage;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

}
