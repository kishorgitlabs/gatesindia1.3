package com.brainmagic.gatescatalog.api.models.oesearchpartno;

import java.util.List;
import java.io.Serializable;

public class OESearchPartNo implements Serializable {
	private String result;
	private List<String> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<String> data){
		this.data = data;
	}

	public List<String> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"OESearchPartNo{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}