package com.brainmagic.gatescatalog.api.models.PriceList;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class PricePojo{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<PriceLists> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<PriceLists> data){
		this.data = data;
	}

	public List<PriceLists> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"PricePojo{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}