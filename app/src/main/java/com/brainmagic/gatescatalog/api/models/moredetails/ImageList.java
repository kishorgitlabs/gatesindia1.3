package com.brainmagic.gatescatalog.api.models.moredetails;

import com.google.gson.annotations.SerializedName;

public class ImageList {
    @SerializedName("ProductthumbURL1")
    String ProductthumbURL1;
    @SerializedName("ProductthumbURL2")
    String ProductthumbURL2;
    @SerializedName("ProductthumbURL3")
    String ProductthumbURL3;
    @SerializedName("ProductthumbURL4")
    String ProductthumbURL4;
    @SerializedName("ProductthumbURL5")
    String ProductthumbURL5;
    @SerializedName("ProductthumbURL6")
    String ProductthumbURL6;
    @SerializedName("ProductthumbURL7")
    String ProductthumbURL7;
    @SerializedName("ProductthumbURL8")
    String ProductthumbURL8;
    @SerializedName("ProductthumbURL9")
    String ProductthumbURL9;
    @SerializedName("ProductthumbURL10")
    String ProductthumbURL10;
    @SerializedName("ProductthumbURL11")
    String ProductthumbURL11;
    @SerializedName("ProductthumbURL12")
    String ProductthumbURL12;
    @SerializedName("ProductthumbURL13")
    String ProductthumbURL13;
    @SerializedName("ProductthumbURL14")
    String ProductthumbURL14;
    @SerializedName("ProductthumbURL15")
    String ProductthumbURL15;
    @SerializedName("ProductthumbURL16")
    String ProductthumbURL16;
    @SerializedName("ProductthumbURL17")
    String ProductthumbURL17;
    @SerializedName("ProductthumbURL18")
    String ProductthumbURL18;
    @SerializedName("ProductthumbURL19")
    String ProductthumbURL19;
    @SerializedName("ProductthumbURL20")
    String ProductthumbURL20;
    @SerializedName("URL1")
    String URL1;
    @SerializedName("URLtype1")
    String URLtype1;
    @SerializedName("URLtype2")
    String URLtype2;
    @SerializedName("URL2")
    String URL2;
    @SerializedName("URLtype3")
    String URLtype3;
    @SerializedName("URL3")
    String URL3;
    @SerializedName("URLtype4")
    String URLtype4;
    @SerializedName("URL4")
    String URL4;

    public String getProductthumbURL1() {
        return ProductthumbURL1;
    }

    public void setProductthumbURL1(String productthumbURL1) {
        ProductthumbURL1 = productthumbURL1;
    }

    public String getProductthumbURL2() {
        return ProductthumbURL2;
    }

    public void setProductthumbURL2(String productthumbURL2) {
        ProductthumbURL2 = productthumbURL2;
    }

    public String getProductthumbURL3() {
        return ProductthumbURL3;
    }

    public void setProductthumbURL3(String productthumbURL3) {
        ProductthumbURL3 = productthumbURL3;
    }

    public String getProductthumbURL4() {
        return ProductthumbURL4;
    }

    public void setProductthumbURL4(String productthumbURL4) {
        ProductthumbURL4 = productthumbURL4;
    }

    public String getProductthumbURL5() {
        return ProductthumbURL5;
    }

    public void setProductthumbURL5(String productthumbURL5) {
        ProductthumbURL5 = productthumbURL5;
    }

    public String getProductthumbURL6() {
        return ProductthumbURL6;
    }

    public void setProductthumbURL6(String productthumbURL6) {
        ProductthumbURL6 = productthumbURL6;
    }

    public String getProductthumbURL7() {
        return ProductthumbURL7;
    }

    public void setProductthumbURL7(String productthumbURL7) {
        ProductthumbURL7 = productthumbURL7;
    }

    public String getProductthumbURL8() {
        return ProductthumbURL8;
    }

    public void setProductthumbURL8(String productthumbURL8) {
        ProductthumbURL8 = productthumbURL8;
    }

    public String getProductthumbURL9() {
        return ProductthumbURL9;
    }

    public void setProductthumbURL9(String productthumbURL9) {
        ProductthumbURL9 = productthumbURL9;
    }

    public String getProductthumbURL10() {
        return ProductthumbURL10;
    }

    public void setProductthumbURL10(String productthumbURL10) {
        ProductthumbURL10 = productthumbURL10;
    }

    public String getProductthumbURL11() {
        return ProductthumbURL11;
    }

    public void setProductthumbURL11(String productthumbURL11) {
        ProductthumbURL11 = productthumbURL11;
    }

    public String getProductthumbURL12() {
        return ProductthumbURL12;
    }

    public void setProductthumbURL12(String productthumbURL12) {
        ProductthumbURL12 = productthumbURL12;
    }

    public String getProductthumbURL13() {
        return ProductthumbURL13;
    }

    public void setProductthumbURL13(String productthumbURL13) {
        ProductthumbURL13 = productthumbURL13;
    }

    public String getProductthumbURL14() {
        return ProductthumbURL14;
    }

    public void setProductthumbURL14(String productthumbURL14) {
        ProductthumbURL14 = productthumbURL14;
    }

    public String getProductthumbURL15() {
        return ProductthumbURL15;
    }

    public void setProductthumbURL15(String productthumbURL15) {
        ProductthumbURL15 = productthumbURL15;
    }

    public String getProductthumbURL16() {
        return ProductthumbURL16;
    }

    public void setProductthumbURL16(String productthumbURL16) {
        ProductthumbURL16 = productthumbURL16;
    }

    public String getProductthumbURL17() {
        return ProductthumbURL17;
    }

    public void setProductthumbURL17(String productthumbURL17) {
        ProductthumbURL17 = productthumbURL17;
    }

    public String getProductthumbURL18() {
        return ProductthumbURL18;
    }

    public void setProductthumbURL18(String productthumbURL18) {
        ProductthumbURL18 = productthumbURL18;
    }

    public String getProductthumbURL19() {
        return ProductthumbURL19;
    }

    public void setProductthumbURL19(String productthumbURL19) {
        ProductthumbURL19 = productthumbURL19;
    }

    public String getProductthumbURL20() {
        return ProductthumbURL20;
    }

    public void setProductthumbURL20(String productthumbURL20) {
        ProductthumbURL20 = productthumbURL20;
    }

    public String getURL1() {
        return URL1;
    }

    public void setURL1(String URL1) {
        this.URL1 = URL1;
    }

    public String getURLtype1() {
        return URLtype1;
    }

    public void setURLtype1(String URLtype1) {
        this.URLtype1 = URLtype1;
    }

    public String getURLtype2() {
        return URLtype2;
    }

    public void setURLtype2(String URLtype2) {
        this.URLtype2 = URLtype2;
    }

    public String getURL2() {
        return URL2;
    }

    public void setURL2(String URL2) {
        this.URL2 = URL2;
    }

    public String getURLtype3() {
        return URLtype3;
    }

    public void setURLtype3(String URLtype3) {
        this.URLtype3 = URLtype3;
    }

    public String getURL3() {
        return URL3;
    }

    public void setURL3(String URL3) {
        this.URL3 = URL3;
    }

    public String getURLtype4() {
        return URLtype4;
    }

    public void setURLtype4(String URLtype4) {
        this.URLtype4 = URLtype4;
    }

    public String getURL4() {
        return URL4;
    }

    public void setURL4(String URL4) {
        this.URL4 = URL4;
    }
}
