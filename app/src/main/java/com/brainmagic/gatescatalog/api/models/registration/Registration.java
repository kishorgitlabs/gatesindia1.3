
package com.brainmagic.gatescatalog.api.models.registration;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Registration {

    @SerializedName("data")
    private RegistrationData mData;
    @SerializedName("result")
    private String mResult;

    public RegistrationData getData() {
        return mData;
    }

    public void setData(RegistrationData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
