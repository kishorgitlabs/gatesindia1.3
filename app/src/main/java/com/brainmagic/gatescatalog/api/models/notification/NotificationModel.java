
package com.brainmagic.gatescatalog.api.models.notification;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class NotificationModel {

    @SerializedName("data")
    private List<NotificationResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<NotificationResult> getData() {
        return mData;
    }

    public void setData(List<NotificationResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
