
package com.brainmagic.gatescatalog.api.models.schemespromo;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SchemesPromoResult {

    @SerializedName("ActiveMessage")
    private String mActiveMessage;
    @SerializedName("Country")
    private Object mCountry;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("deleteflag")
    private String mDeleteflag;
    @SerializedName("Description")
    private String mDescription;
    @SerializedName("SchemeNo")
    private Object mSchemeNo;
    @SerializedName("SchemesPromotionid")
    private Long mSchemesPromotionid;
    @SerializedName("Schemesname")
    private String mSchemesname;
    @SerializedName("ShemeExpiryDate")
    private String mShemeExpiryDate;
    @SerializedName("updatetime")
    private Object mUpdatetime;
    @SerializedName("Usertype")
    private String mUsertype;

    public String getActiveMessage() {
        return mActiveMessage;
    }

    public void setActiveMessage(String activeMessage) {
        mActiveMessage = activeMessage;
    }

    public Object getCountry() {
        return mCountry;
    }

    public void setCountry(Object country) {
        mCountry = country;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDeleteflag() {
        return mDeleteflag;
    }

    public void setDeleteflag(String deleteflag) {
        mDeleteflag = deleteflag;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Object getSchemeNo() {
        return mSchemeNo;
    }

    public void setSchemeNo(Object schemeNo) {
        mSchemeNo = schemeNo;
    }

    public Long getSchemesPromotionid() {
        return mSchemesPromotionid;
    }

    public void setSchemesPromotionid(Long schemesPromotionid) {
        mSchemesPromotionid = schemesPromotionid;
    }

    public String getSchemesname() {
        return mSchemesname;
    }

    public void setSchemesname(String schemesname) {
        mSchemesname = schemesname;
    }

    public String getShemeExpiryDate() {
        return mShemeExpiryDate;
    }

    public void setShemeExpiryDate(String shemeExpiryDate) {
        mShemeExpiryDate = shemeExpiryDate;
    }

    public Object getUpdatetime() {
        return mUpdatetime;
    }

    public void setUpdatetime(Object updatetime) {
        mUpdatetime = updatetime;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
