
package com.brainmagic.gatescatalog.api.models.background;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class BackgroundData {

    @SerializedName("City")
    private String mCity;
    @SerializedName("Devicetype")
    private String mDevicetype;
    @SerializedName("Id")
    private Long mId;
    @SerializedName("Insertdate")
    private String mInsertdate;
    @SerializedName("insertdate1")
    private String mInsertdate1;
    @SerializedName("Location")
    private String mLocation;
    @SerializedName("Mobileno")
    private String mMobileno;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Partno")
    private String mPartno;
    @SerializedName("Segment")
    private String mSegment;
    @SerializedName("State")
    private String mState;
    @SerializedName("Usertype")
    private String mUsertype;
    @SerializedName("VehicleModel")
    private String mVehicleModel;
    @SerializedName("Zipcode")
    private String mZipcode;

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getDevicetype() {
        return mDevicetype;
    }

    public void setDevicetype(String devicetype) {
        mDevicetype = devicetype;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getInsertdate() {
        return mInsertdate;
    }

    public void setInsertdate(String insertdate) {
        mInsertdate = insertdate;
    }

    public String getInsertdate1() {
        return mInsertdate1;
    }

    public void setInsertdate1(String insertdate1) {
        mInsertdate1 = insertdate1;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String getMobileno() {
        return mMobileno;
    }

    public void setMobileno(String mobileno) {
        mMobileno = mobileno;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getPartno() {
        return mPartno;
    }

    public void setPartno(String partno) {
        mPartno = partno;
    }

    public String getSegment() {
        return mSegment;
    }

    public void setSegment(String segment) {
        mSegment = segment;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

    public String getVehicleModel() {
        return mVehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        mVehicleModel = vehicleModel;
    }

    public String getZipcode() {
        return mZipcode;
    }

    public void setZipcode(String zipcode) {
        mZipcode = zipcode;
    }

}
