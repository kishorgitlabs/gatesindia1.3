
package com.brainmagic.gatescatalog.api.models.checkregister;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class CheckRegister {

    @SerializedName("data")
    private CheckRegisterData mData;
    @SerializedName("result")
    private String mResult;

    public CheckRegisterData getData() {
        return mData;
    }

    public void setData(CheckRegisterData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
