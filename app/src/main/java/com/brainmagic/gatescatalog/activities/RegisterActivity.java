package com.brainmagic.gatescatalog.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.brainmagic.gatescatalog.activities.productsearch.Product_Activity;
import com.brainmagic.gatescatalog.gates.R;
import com.brainmagic.gatescatalog.alertbox.Alertbox;
import com.brainmagic.gatescatalog.api.APIService;
import com.brainmagic.gatescatalog.api.RetrofitClient;
import com.brainmagic.gatescatalog.api.models.checkregister.CheckRegister;
import com.brainmagic.gatescatalog.api.models.registration.Registration;
import com.brainmagic.gatescatalog.api.models.sendotp.SendOTP;
import com.brainmagic.gatescatalog.api.models.webotpverify.WebOTPVerify;
import com.brainmagic.gatescatalog.font.ButtonFontDesign;
import com.brainmagic.gatescatalog.gcm.GCMClientManager;
import com.brainmagic.gatescatalog.networkconnection.Network_Connection_Activity;
import com.brainmagic.gatescatalog.services.BackgroundService;
import com.goodiebag.pinview.Pinview;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.android.gms.plus.PlusOneDummyView.TAG;


public class RegisterActivity extends Activity {
    // ImageView mail,call;
    EditText name, email, mobileno;
    EditText country, state, city, zipCode, streetName;
    Pinview pinviewregister;
    Button register, generateotp;
    Spinner usertype;
    ProgressDialog progressDialog;
    private LinearLayout gstinLayout, shopNameLayout;
    String Name, Email, Mobno, Usertype, Businessname, latitute, longi;
    String[] types = {"Type Of User", "Distributors", "Dealer / Retailer", "Mechanic", "Gates Employee", "Others"};
    Connection connection;
    Statement stmt;
    CheckBox checkbox;
    ResultSet resultSet;
    SharedPreferences myshare;
    SharedPreferences.Editor editor;
    String subsc;
    String status = "Active";
    String Dbstatus = "";
    String formattedDate;
    // String PROJECT_NUMBER= "97108778128";
    String PROJECT_NUMBER = "97108778128";
    String regID, select_email, select_phno, dis_select_phno, select_name, mOtp;
    String Msg = "";
    private EditText addressss, businessname;
    private boolean permissionGranted = true;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private SettingsClient mSettingsClient;
    private Boolean mRequestingLocationUpdates;
    private FusedLocationProviderClient mFusedLocationClient;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 50000;
    private static final int REQUEST_CHECK_SETTINGS = 100;
    private static final int REQUEST_CHECK_SETTINGS_GPS = 100;
    private String locationaddress;
    private Boolean otpgeneratedreg = false;
    private List<String> otp = new ArrayList<>();
    private String lat, longitu;
    private TextView otpmsg2, otpmsg1;
    private String webverifymobile, webusertype, otpentered;
    Alertbox alertbox;
    private String State, City, ZipCode;

    @Override
    protected void onStart() {
        super.onStart();
        View view = this.getCurrentFocus();

        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        alertbox = new Alertbox(RegisterActivity.this);
        
     /*   mail=(ImageView)findViewById(R.id.mail);
        call=(ImageView)findViewById(R.id.call);*/

        addressss = findViewById(R.id.addressss);
        otpmsg2 = findViewById(R.id.otpmsg2);
        otpmsg1 = findViewById(R.id.otpmsg1);
        businessname = findViewById(R.id.businessname);
        generateotp = findViewById(R.id.generateotp);
        pinviewregister = findViewById(R.id.pinviewregister);
        gstinLayout = findViewById(R.id.gstin_layout);
        shopNameLayout = findViewById(R.id.shop_name_layout);

        country = findViewById(R.id.country_edit);
        state = findViewById(R.id.state_edit);
        city = findViewById(R.id.city_edit);
        zipCode = findViewById(R.id.zip_code_edit);
        streetName = findViewById(R.id.street_edit);

        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);

        mobileno = (EditText) findViewById(R.id.mobileno);

        register = (Button) findViewById(R.id.register);
        usertype = (Spinner) findViewById(R.id.usertype);

        ButtonFontDesign fontDesign = new ButtonFontDesign(this);
//        generateotp.setTypeface(fontDesign.setTypeface());
//        register.setTypeface(fontDesign.setTypeface());


        Date d = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(d);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(c.getTime());

        generateotp.setVisibility(View.VISIBLE);


        progressDialog = new ProgressDialog(RegisterActivity.this);
        // spinner (wheel) style dialog
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading...");

        myshare = getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        webverifymobile = myshare.getString("webverify", "");
        webusertype = myshare.getString("usertypetoverify", "");

        ArrayAdapter<String> xxx = new ArrayAdapter<String>(RegisterActivity.this, android.R.layout.simple_list_item_1, types);
        usertype.setAdapter(xxx);


        usertype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                Usertype = adapterView.getItemAtPosition(i).toString();
               /* if (Usertype.equals("Dealer / Retailer")) {
                    gstinLayout.setVisibility(View.VISIBLE);
                    shopNameLayout.setVisibility(View.VISIBLE);
                    alertbox.showAlert("Pls enter GSTIN number, this will help to activate your account very quickly for Loyalty Reward Points");
                } else {
                    gstinLayout.setVisibility(View.GONE);
                    shopNameLayout.setVisibility(View.GONE);
                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mobileno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mobileno.length() == 10) {
                    Mobno = mobileno.getText().toString();
//                checkuserexists();
                    try {
                        final ProgressDialog loading = ProgressDialog.show(RegisterActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
                        APIService service = RetrofitClient.getApiService();
//            lat=String.valueOf(mCurrentLocation.getLatitude());
//            longi=String.valueOf(mCurrentLocation.getLongitude());
                        Call<CheckRegister> call = service.checkreg(Mobno);
                        call.enqueue(new Callback<CheckRegister>() {
                            @Override
                            public void onResponse(Call<CheckRegister> call, final Response<CheckRegister> response) {
                                try {
                                    if (response.body().getResult().equals("Success")) {
                                        loading.dismiss();
                                        name.setText(response.body().getData().getName());
                                        email.setText(response.body().getData().getEmail());
                                        businessname.setText(response.body().getData().getBusinessName());
                                        state.setText(response.body().getData().getmState());
                                        city.setText(response.body().getData().getmCity());
                                        ArrayList usertypes = new ArrayList();
                                        usertypes.add("Distributors");
                                        usertypes.add("Dealer / Retailer");
                                        usertypes.add("Mechanic");
                                        usertypes.add("Gates Employee");
                                        usertypes.add("Others");
                                        if (usertypes.contains(response.body().getData().getUsertype())) {
                                            usertypes.remove(response.body().getData().getUsertype());
                                        }
                                        usertypes.add(0, response.body().getData().getUsertype());
                                        ArrayAdapter<String> xxx = new ArrayAdapter<String>(RegisterActivity.this, android.R.layout.simple_list_item_1, usertypes);
                                        usertype.setAdapter(xxx);

                                        if (response.body().getData().getOTPStatus().equalsIgnoreCase("Verified")) {
                                            generateotp.setVisibility(View.GONE);

                                            name.setEnabled(false);
                                            email.setEnabled(false);
                                            businessname.setEnabled(false);
                                            usertype.setEnabled(false);

                                            register.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {

                                                    String nameLog = response.body().getData().getName();
                                                    loading.dismiss();
                                                    editor.putBoolean("isregister", true);
//                                                    editor.putString("Email", Email);
//                                                    editor.putString("MobNo", Mobno);
//                                                    editor.putString("Usertype", Usertype);
                                                    editor.putString("NameCheck", Name);
//                                                    editor.putString("Bname", "");
                                                    editor.putString("Email", response.body().getData().getEmail());
                                                    editor.putString("MobNo", response.body().getData().getMobileno());
                                                    editor.putString("Usertype", response.body().getData().getUsertype());
                                                    editor.putString("NameCheck", response.body().getData().getName());
                                                    editor.putString("Bname", response.body().getData().getBusinessName());
                                                    editor.putString("city", response.body().getData().getmCity());
                                                    editor.putString("state", response.body().getData().getmState());
                                                    editor.putString("zipCode", response.body().getData().getmZipCode());
                                                    editor.putString("streetName", response.body().getData().getAddress());
                                                    editor.putString("nameLogs",nameLog);
                                                    editor.commit();




                                                    String address = response.body().getData().getAddress();

//                                                    sendLog("Registration", nameLog, "", "", Mobno, Usertype,
//                                                            State, City, ZipCode, address, "Android");

                                                    Intent go = new Intent(RegisterActivity.this, HomeScreenActivity.class);
                                                    go.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                    startActivity(go);
                                                    finish();
                                                }
                                            });
                                        } else if (response.body().getData().getOTPStatus().equalsIgnoreCase("NotVerified")) {
                                            generateotp.setVisibility(View.VISIBLE);
                                            name.setClickable(true);
                                            name.setEnabled(true);
                                            name.setPressed(true);

                                            email.setClickable(true);
                                            email.setEnabled(true);
                                            email.setPressed(true);

                                            businessname.setClickable(true);
                                            businessname.setEnabled(true);
                                            businessname.setPressed(true);

                                            usertype.setEnabled(true);
                                        }
                                    } else if (response.body().getResult().equals("NotSuccess")) {
                                        loading.dismiss();
//                            Nointernet("Please Try Again Later");
                                    } else if (response.body().getResult().equals("No Record")) {
                                        loading.dismiss();
                                        generateotp.setVisibility(View.VISIBLE);
                                        name.getText().clear();
                                        email.getText().clear();
                                        businessname.getText().clear();

                                        name.setEnabled(true);
                                        email.setEnabled(true);
                                        businessname.setEnabled(true);
                                        usertype.setEnabled(true);
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    loading.dismiss();
                                    Nointernet("Please Try Again Later");
                                }
                            }

                            @Override
                            public void onFailure(Call<CheckRegister> call, Throwable t) {
                                loading.dismiss();
                                t.printStackTrace();
                                Nointernet(getString(R.string.server_error));
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        generateotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String PhoneNo = mobileno.getText().toString();
                Name = name.getText().toString();
                Email = email.getText().toString();
                Mobno = mobileno.getText().toString();
                Businessname = businessname.getText().toString();
                String Regex = "[^\\d]";
                String PhoneDigits = PhoneNo.replaceAll(Regex, "");
                if (PhoneDigits.length() != 10) {
                    Toast.makeText(RegisterActivity.this, "Mobile Number Must be 10 digits", Toast.LENGTH_SHORT).show();
                } else if (Usertype.equals("Type Of User")) {
                    Toast.makeText(RegisterActivity.this, "Select User Type", Toast.LENGTH_LONG).show();
                } else if (Name.equals("")) {
                    name.setFocusable(true);
                    name.setError("Enter Your Name");
                } else if (Businessname.equals("")) {
                    Toast toast = Toast.makeText(RegisterActivity.this, "Enter Business Name", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else {
                    Mobno = mobileno.getText().toString();
                    Name = name.getText().toString();
                    sendOtp();
                    pinviewregister.setVisibility(View.VISIBLE);
                    otpmsg1.setVisibility(View.VISIBLE);
                    otpmsg2.setVisibility(View.VISIBLE);
                    generateotp.setText("Resend OTP");
                    otpgeneratedreg = true;
                }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Name = name.getText().toString();
                Email = email.getText().toString();
                Mobno = mobileno.getText().toString();
                Businessname = businessname.getText().toString();
                locationaddress = addressss.getText().toString();
                otpentered = pinviewregister.getValue();
                String PhoneNo = mobileno.getText().toString();
                String Regex = "[^\\d]";
                String PhoneDigits = PhoneNo.replaceAll(Regex, "");

                if (PhoneDigits.length() != 10) {
                    Toast.makeText(RegisterActivity.this, "Mobile Number Must be 10 digits", Toast.LENGTH_SHORT).show();
                } else if (Usertype.equals("Type Of User")) {
                    Toast.makeText(RegisterActivity.this, "Select User Type", Toast.LENGTH_LONG).show();
                } else if (Name.equals("")) {
                    name.setFocusable(true);
                    name.setError("Enter Your Name");
                } else if (Businessname.equals("")) {
                    Toast toast = Toast.makeText(RegisterActivity.this, "Enter Business Name", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else if (!otpgeneratedreg) {
                    Toast toast = Toast.makeText(RegisterActivity.this, "Please click on Generate Button to Generate OTP", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                } else {
                    reid();

                }
            }
        });

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        initPermission();
//        init();
//        startLocationUpdate();
        if (!webverifymobile.equals("") && !webusertype.equals("")) {
            checkverification();
        }

    }

    private void sendLog(String segment, String userName, String vehicleName, String partNo, String mobileNumber, String userTypes, String stateLog,
                         String CityLog, String ZipCodeLog, String location, String deviceType)
    {
        Network_Connection_Activity network_connection_activity = new Network_Connection_Activity(RegisterActivity.this);
        if (network_connection_activity.CheckInternet()) {
            startService(new Intent(RegisterActivity.this, BackgroundService.class)
                    .putExtra("segmentLog", segment)
                    .putExtra("nameLog", userName)
                    .putExtra("vehicleNameLog", vehicleName)
                    .putExtra("partNoLog", partNo)
                    .putExtra("mobileNoLog", mobileNumber)
                    .putExtra("userTypeLog", userTypes)
                    .putExtra("stateLog", stateLog)
                    .putExtra("cityLog", CityLog)
                    .putExtra("zipCodeLog", ZipCodeLog)
                    .putExtra("locationLog", location)
                    .putExtra("deviceTypeLog", deviceType)
            );
        }
    }

    private void checkverification() {
        try {
            final ProgressDialog loading = ProgressDialog.show(RegisterActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
//            lat=String.valueOf(mCurrentLocation.getLatitude());
//            longi=String.valueOf(mCurrentLocation.getLongitude());
            Call<WebOTPVerify> call = service.webverify(webverifymobile, webusertype);
            call.enqueue(new Callback<WebOTPVerify>() {
                @Override
                public void onResponse(Call<WebOTPVerify> call, Response<WebOTPVerify> response) {
                    try {
                        if (response.body().getResult().equals("Verified")) {
                            loading.dismiss();
                            editor.putBoolean("isregister", true);
                            editor.putString("Email", response.body().getData().getEmail());
                            editor.putString("MobNo", response.body().getData().getMobileno());
                            editor.putString("Usertype", response.body().getData().getUsertype());
                            editor.putString("NameCheck", response.body().getData().getName());
                            editor.putString("Bname", response.body().getData().getBusinessName());
                            editor.commit();
                            editor.apply();
                            Intent home = new Intent(getApplicationContext(), HomeScreenActivity.class);
                            home.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(home);
                        } else if (response.body().getResult().equals("NotVerified")) {
                            loading.dismiss();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<WebOTPVerify> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendOtp() {
        try {
            final ProgressDialog loading = ProgressDialog.show(RegisterActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
//            lat=String.valueOf(mCurrentLocation.getLatitude());
//            longi=String.valueOf(mCurrentLocation.getLongitude());
            Call<SendOTP> call = service.sendtop(Mobno, Name, Usertype, Email, Businessname, locationaddress);
            call.enqueue(new Callback<SendOTP>() {
                @Override
                public void onResponse(Call<SendOTP> call, Response<SendOTP> response) {
                    try {
                        if (response.body().getResult().equals("Success")) {
                            loading.dismiss();
                            editor.putString("webverify", Mobno);
                            editor.putString("usertypetoverify", Usertype);
                            editor.commit();
                            editor.apply();
                            otp.add(response.body().getData());
                        }
                        else if (response.body().getResult().equals("Notsuccess")) {
                            loading.dismiss();
                            Nointernet("Please Try Again Later");
                        }

                    }
                    catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<SendOTP> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkuserexists() {
        try {
            final ProgressDialog loading = ProgressDialog.show(RegisterActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
//            lat=String.valueOf(mCurrentLocation.getLatitude());
//            longi=String.valueOf(mCurrentLocation.getLongitude());
            Call<CheckRegister> call = service.checkreg(Mobno);
            call.enqueue(new Callback<CheckRegister>() {
                @Override
                public void onResponse(Call<CheckRegister> call, Response<CheckRegister> response) {
                    try {
                        if (response.body().getResult().equals("Success")) {
                            loading.dismiss();
                            name.setText(response.body().getData().getName());
                            email.setText(response.body().getData().getEmail());
                            businessname.setText(response.body().getData().getBusinessName());
//                           String usertyper1=response.body().getData().getUsertype();
                            ArrayList usertypes = new ArrayList();
                            usertypes.add("Distributors");
                            usertypes.add("Dealer / Retailer");
                            usertypes.add("Mechanic");
                            usertypes.add("Gates Employee");
                            usertypes.add("Others");
                            if (usertypes.contains(response.body().getData().getUsertype())) {
                                usertypes.remove(response.body().getData().getUsertype());
                            }
                            usertypes.add(0, response.body().getData().getUsertype());
                            ArrayAdapter<String> xxx = new ArrayAdapter<String>(RegisterActivity.this, android.R.layout.simple_list_item_1, usertypes);
                            usertype.setAdapter(xxx);

                            if (response.body().getData().getOTPStatus().equalsIgnoreCase("Verified")) {
                                generateotp.setVisibility(View.GONE);
                            } else if (response.body().getData().getOTPStatus().equalsIgnoreCase("NotVerified")) {
                                generateotp.setVisibility(View.VISIBLE);
                            }
                        } else if (response.body().getResult().equals("NotSuccess")) {
                            loading.dismiss();
//                            Nointernet("Please Try Again Later");
                        } else {
                            loading.dismiss();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
//                        Nointernet("Please Try Again Later");
                    }
                }

                @Override
                public void onFailure(Call<CheckRegister> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
//                    Nointernet("Please Try Again Later");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void reid() {
        GCMClientManager pushClientManager = new GCMClientManager(RegisterActivity.this, PROJECT_NUMBER);
        pushClientManager.registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
            @Override
            public void onSuccess(String registrationId, boolean isNewRegistration) {

                Log.d("Registration id", registrationId);
                //send this registrationId to your server
                regID = registrationId;
                checkinternet();
            }

            @Override
            public void onFailure(String ex) {
                super.onFailure(ex);
            }
        });
    }

    private void initPermission() {
        if (PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(RegisterActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100);
//            init();
        } else {
            permissionGranted = true;
            init();
            startLocationUpdate();
        }
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, final String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            int len = permissions.length;
            for (int i = 0; i < len; i++) {
                if (ActivityCompat.checkSelfPermission(RegisterActivity.this, permissions[i]) != PackageManager.PERMISSION_GRANTED) {
                    permissionGranted = false;
                }
            }

            if (permissionGranted) {
//                permissionGranted = true;
                init();
//            if(isLocationSuccess)

                startLocationUpdate();

            } else {

                AlertDialog.Builder dialog = new AlertDialog.Builder(RegisterActivity.this);
                dialog.setTitle("Location");
                dialog.setMessage("You did not give permission to access your Location. Do want to exit");
                dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

                dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestManualPermission(permissions, requestCode);
                        permissionGranted = true;
                    }
                });
                dialog.show();
            }
        }
    }

    private void requestManualPermission(String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(this, permissions, requestCode);
    }

    private void regiStration() {
        try {
            final ProgressDialog loading = ProgressDialog.show(RegisterActivity.this, getResources().getString(R.string.app_name), "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            lat = String.valueOf(mCurrentLocation.getLatitude());
            longi = String.valueOf(mCurrentLocation.getLongitude());
            Call<Registration> call = service.newregister(Usertype, Mobno, status, Name, Email, regID, "Verified", formattedDate, "", "India", otpentered, Businessname, lat, longi, locationaddress, State, City, ZipCode);
            call.enqueue(new Callback<Registration>() {
                @Override
                public void onResponse(Call<Registration> call, final Response<Registration> response) {
                    try {
                        if (response.body().getResult().equals("Success")) {
                            loading.dismiss();
                            final String na = response.body().getData().getName();
                            final AlertDialog alertDialogbox = new AlertDialog.Builder(RegisterActivity.this).create();
                            LayoutInflater inflater = RegisterActivity.this.getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.alertbox, null);
                            alertDialogbox.setView(dialogView);
                            ImageView sucess = (ImageView) dialogView.findViewById(R.id.success);
                            TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                            Button okay = (Button) dialogView.findViewById(R.id.okay);
                            log.setText("Registration Successfull");
                            sucess.setImageResource(R.drawable.checked);
                            okay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View arg0) {
                                    // TODO Auto-generated method stub
                                    editor.putBoolean("isregister", true);
                                    editor.putString("Email", Email);
                                    editor.putString("MobNo", Mobno);
                                    editor.putString("Usertype", Usertype);
                                    editor.putString("NameCheck", Name);
                                    editor.putString("Bname", Businessname);
                                    /*editor.putString("Email", email.getText().toString());
                                    editor.putString("MobNo", mobileno.getText().toString());
                                    editor.putString("Usertype", usertype.getSelectedItem().toString());
                                    editor.putString("Name", name.getText().toString());
                                    editor.putString("Bname", businessname.getText().toString());*/
                                    editor.putString("city", city.getText().toString());
                                    editor.putString("state", state.getText().toString());
                                    editor.putString("zipCode", zipCode.getText().toString());
                                    editor.putString("streetName", streetName.getText().toString());
                                    editor.commit();
//
//                                    sendLog("Registration", Name, "", "", Mobno, Usertype,
//                                            state.getText().toString(), city.getText().toString(), zipCode.getText().toString(),
//                                            streetName.getText().toString(),
//                                            "Android");

                                    Intent go = new Intent(RegisterActivity.this, HomeScreenActivity.class);
                                    go.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(go);
                                    alertDialogbox.dismiss();
                                }
                            });
                            alertDialogbox.show();
                        } else if (response.body().getResult().equals("OTPInvalid")) {
                            loading.dismiss();
                            Toast toast = Toast.makeText(RegisterActivity.this, "InValid OTP", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        } else if (response.body().getResult().equals("UpdateSuccess")) {
                            loading.dismiss();
                            final AlertDialog alertDialogbox = new AlertDialog.Builder(
                                    RegisterActivity.this).create();

                            LayoutInflater inflater = RegisterActivity.this.getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.alertbox, null);
                            alertDialogbox.setView(dialogView);
                            ImageView sucess = (ImageView) dialogView.findViewById(R.id.success);
                            TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                            Button okay = (Button) dialogView.findViewById(R.id.okay);
                            log.setText("Registration Successfull");
                            sucess.setImageResource(R.drawable.checked);
                            okay.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View arg0) {
                                    // TODO Auto-generated method stub
                                    editor.putBoolean("isregister", true);
                                    editor.putString("Email", Email);
                                    editor.putString("MobNo", Mobno);
                                    editor.putString("Usertype", Usertype);
                                    editor.putString("NameCheck", Name);
                                    editor.putString("Bname", "");
                                    editor.commit();
                                    Intent go = new Intent(RegisterActivity.this, HomeScreenActivity.class);
                                    go.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(go);
                                    alertDialogbox.dismiss();
                                }
                            });
                            alertDialogbox.show();
                        } else if (response.body().getResult().equals("NotDistributor")) {
                            loading.dismiss();
                            Nointernet("The data provided is not matching with our Distributor database, Kindly check & try again. \n\n\n" +
                                    "If you are not registered as a Distributor,Please contact Gates Unitta \n CC-AAMI@gates.com \n or \n 044 – 47115100 / 044 – 47115279 / 044 – 47115281 \n for registration ");
                        } else if (response.body().getResult().equals("AlreadyRegisteranotherUserType")) {
                            loading.dismiss();
                            Nointernet("Your Mobile Number Already Registered in another usertype");
                        } else {
                            loading.dismiss();
                            Nointernet("Please Try Again Later");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<Registration> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
//                        startLocationUpdate();
                        if (permissionGranted) {
                            startLocationUpdate();
                        } else {
                            initPermission();
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(RegisterActivity.this);
                        alertDialog.setMessage("If you don't enable GPS, Registration Not To be Done Please Enable GPS");
                        alertDialog.setTitle("Gates Catalog");

                        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });

                        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                initPermission();
                            }
                        });
                        alertDialog.setCancelable(false);
                        alertDialog.show();
                        break;
                    default:
                        finish();
                        break;
                }
                break;
        }
    }

    private void init() {

        if (permissionGranted) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mSettingsClient = LocationServices.getSettingsClient(this);

            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    // location is received
                    mCurrentLocation = locationResult.getLastLocation();
//                    mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                    updateLocationUI();
                }
            };

            mRequestingLocationUpdates = false;
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setFastestInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            mLocationSettingsRequest = builder.build();
//        startLocationButtonClick();
        }
    }


    private void startLocationUpdate() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

//                        Toast.makeText(getApplicationContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());


//                        LocationManager manager= (LocationManager) getSystemService(LOCATION_SERVICE);


                        updateLocationUI();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(RegisterActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(RegisterActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                                break;
                            case LocationSettingsStatusCodes.SUCCESS:
                                permissionGranted = true;
                                init();
                        }
                        updateLocationUI();
                    }
                });
    }


    private void updateLocationUI() {
        if (mCurrentLocation != null) {
//            addressss.setText(
//                    "Lat: " + mCurrentLocation.getLatitude() + ", " +
//                            "Lng: " + mCurrentLocation.getLongitude()
//            );
            new GeocodeAsyncTask().execute(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        }
    }

    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";

        @SuppressLint("LongLogTag")
        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(RegisterActivity.this, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);

                    Log.d(TAG, "doInBackground: ************");
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }
                if (addresses != null && addresses.size() > 0)

                    return addresses.get(0);


            }

            return null;
        }

        @SuppressLint("LongLogTag")
        protected void onPostExecute(Address addresss) {

            if (addresss == null) {
//                new GetGeoCodeAPIAsynchTask().execute(mylocation.getLatitude(), mylocation.getLongitude());
                Log.d(TAG, "onPostExecute: *****");
            } else {
//                progressBar.setVisibility(View.GONE);
                String address = addresss.getAddressLine(0);
                editor.putString("ToAddress", address);
                editor.commit();
                editor.apply();


                String StreetName = addresss.getAddressLine(0);
                streetName.setText(StreetName);
                editor.putString("StreetName", StreetName);
                editor.commit();
                editor.apply();

                City = addresss.getLocality();
                city.setText(City);
                editor.putString("City", City);
                editor.commit();
                editor.apply();

                Log.d(TAG, "onPostExecute: **************************" + City);
                String city = addresss.getLocality();
                State = addresss.getAdminArea();
                state.setText(State);
                editor.putString("State", State);
                editor.commit();
                editor.apply();

                String Country = addresss.getCountryName();
                country.setText(Country);
                editor.putString("Country", Country);
                editor.commit();
                editor.apply();


                ZipCode = addresss.getPostalCode();
                zipCode.setText(ZipCode);
                editor.putString("ZipCode", ZipCode);
                editor.commit();
                editor.apply();

                //create your custom title
                String title = city + "-" + State;
                addressss.setText(address +
                        "\n"
                        + title);
                locationaddress = addressss.getText().toString();
                editor.putString("FromAddress", address);
                Geocoder geocoder = new Geocoder(RegisterActivity.this);
                try {
                    ArrayList<Address> addresses = (ArrayList<Address>) geocoder.getFromLocationName("karur", 50);
                    for (Address address3 : addresses) {
                        double lat = address3.getLatitude();
                        double lon = address3.getLongitude();
//                        address2.setText(lat +
//                                "\n"
//                                + lon);

                    }
                } catch (IOException e) {
                    e.printStackTrace();

                }
                //create your custom title
//                String title = city + "-" + state;
//                Alertbox alertbox=new Alertbox(MainActivity.this);
//                alertbox.showAlertboxwithback("Your Current location is "+city);


            }
        }
    }

    //to check location is enabled or not
    private boolean CheckGateEmail(String usertype) {

        String[] parts = usertype.split("@");
        String part2 = parts[1];
        if (part2.equals("gates.com")) {
            return true;
        } else {
            return false;
        }

    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    private void checkinternet() {
        Network_Connection_Activity connection = new Network_Connection_Activity(RegisterActivity.this);

        if (connection.CheckInternet()) {
            regiStration();
//            progressDialog.show();
//            if (Usertype.equals("Distributors"))
//            {
//                new Distributor().execute();
//            }
//            else
//            {
//
//                new AlreadyRegister().execute();
//            }


        } else {
            // Toast.makeText(RegisterActivity.this,"Not unable to connect please check your Internet connection ! ", Toast.LENGTH_SHORT).show();
            Nointernet(" Unable to connect Internet. please check your Internet connection ! ");


        }


    }


    private void Nointernet(String s) {
        final AlertDialog alertDialog = new AlertDialog.Builder(
                RegisterActivity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertbox, null);
        alertDialog.setView(dialogView);
        ImageView sucess = (ImageView) dialogView.findViewById(R.id.success);
        TextView log = (TextView) dialogView.findViewById(R.id.textView1);
        Button okay = (Button) dialogView.findViewById(R.id.okay);
        log.setText(s);
        sucess.setImageResource(R.drawable.nointernet);

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}



